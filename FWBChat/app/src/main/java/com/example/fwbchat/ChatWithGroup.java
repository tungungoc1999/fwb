package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fwbchat.Adapter.MessageAdapter;
import com.example.fwbchat.Entity.Message;
import com.example.fwbchat.Model.MessageModel;

import java.sql.SQLException;
import java.util.ArrayList;

public class ChatWithGroup extends AppCompatActivity {

    private ArrayList<Message> mlist;
    private MessageAdapter mAdapter;
    private MessageModel DAO = new MessageModel();
    private ImageView btnSend;
    private TextView textMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_with_group);
        TextView tv_name = findViewById(R.id.tv_name);
        ListView lv1= findViewById(R.id.list_mess);
        btnSend = findViewById(R.id.ivSend);
        textMessage = findViewById(R.id.etComposeBox);
        mlist = new ArrayList<Message>();
        lv1.setDivider(null);
        lv1.setDividerHeight(0);
        SharedPreferences preferences = getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        String uid = preferences.getString("uid","");
        String name = preferences.getString("ten","");
        String idConversation = preferences.getString("idGroup","");
        String nameConversation = preferences.getString("nameGroup","");

        tv_name.setText(nameConversation);
        try {
            if(uid !="") {
                mlist = DAO.getListMessageGroup(idConversation);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        mAdapter = new MessageAdapter(ChatWithGroup.this, mlist);
        lv1.setAdapter(mAdapter);
        lv1.setSelection(mAdapter.getCount()-1);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mess = textMessage.getText().toString();
                if(!mess.isEmpty()){
                    Message obj = new Message();
                    obj.setIdNguoiGui(uid);
                    obj.setTenNguoiGui(name);
                    obj.setIdNguoiNhan(idConversation);
                    obj.setTenNguoiNhan(nameConversation);
                    obj.setMessage(mess);
                    obj.setKieuTinNhan("Group");
                    obj.setThoiGianGui("2021-02-26 13:47:00.000");
                    obj.setTrangThai("Đã xem");
                    obj.setCountChuaDoc(1);
                    try {
                        if(DAO.Insert(obj)){
                            mlist = DAO.getListMessageGroup(idConversation);
                            mAdapter = new MessageAdapter(ChatWithGroup.this, mlist);
                            lv1.setAdapter(mAdapter);
                            lv1.setSelection(mAdapter.getCount()-1);
                            textMessage.setText("");
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });
    }
    public void backToListGroup(View view) {
        startActivity(new Intent(ChatWithGroup.this,HomeActivity.class));
    }
    public void detailGroup(View view) {
        startActivity(new Intent(ChatWithGroup.this,DetailGroup.class));
    }
}