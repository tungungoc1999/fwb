package com.example.fwbchat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;

import com.example.fwbchat.Adapter.UserAdapter;
import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.UserModel;

import java.sql.SQLException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentConversation extends ListFragment {
    private ArrayList<User> mlist;
    private UserAdapter mAdapter;
    private View view;
    private UserModel DAO = new UserModel();
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mlist = new ArrayList<User>();
        SharedPreferences preferences = this.getActivity().getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        String uid = preferences.getString("uid","");
        try {
            if(uid !=""){
                mlist = DAO.getuserlist(uid);
            }else{
                mlist = DAO.getuserlist();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
//        mAdapter = new UserAdapter(getActivity(), android.R.layout.simple_list_item_1,mlist);
//        setListAdapter(mAdapter);
//        lv1.setAdapter(mAdapter);
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,mlist);
        setListAdapter(adapter);
        return inflater.inflate(R.layout.list_item_conversation,container,false);
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = getActivity().getSharedPreferences("userLogin",MODE_PRIVATE);
        editor = sharedPref.edit();
        User user = mlist.get(position);
        editor.putString("idConversation",user.getUid());
        editor.putString("nameConversation",user.getName());
        editor.commit();
        Intent intent = new Intent(getActivity(),ChatWithConversation.class);
        startActivity(intent);
        super.onListItemClick(l, v, position, id);
    }
}
