package com.example.fwbchat.Model;

import com.example.fwbchat.DAO.JDBCModel;
import com.example.fwbchat.Entity.Message;
import com.example.fwbchat.Entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MessageModel {
    private JDBCModel jdbcController = new JDBCModel();
    private Connection connection;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    public MessageModel() {
        connection = JDBCModel.Connect(); // Tạo kết nối tới database
    }

    public ArrayList<Message> getListMessage(String id,String idConver,String type) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<Message> list = new ArrayList<>();
        String sql = "SELECT * FROM Messages WHERE IdNguoiGui = ? AND IdNguoiNhan = ? OR IdNguoiNhan = ? AND IdNguoiGui = ? AND KieuTinNhan = ? ORDER BY Id";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        ps.setString(1, id);
        ps.setString(2, idConver);
        ps.setString(3, id);
        ps.setString(4, idConver);
        ps.setString(5, type);

        rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Message(rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getString(9),
                    rs.getInt(10)));// Đọc dữ liệu từ ResultSet
        }
        connection.close();// Đóng kết nối
        return list;
    }
    public ArrayList<Message> getListMessageGroup(String idConver) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<Message> list = new ArrayList<>();
        String sql = "SELECT * FROM Messages WHERE KieuTinNhan = 'Group' AND IdNguoiNhan = ? ORDER BY Id";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        ps.setString(1, idConver);
        rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Message(rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getString(9),
                    rs.getInt(10)));// Đọc dữ liệu từ ResultSet
        }
        connection.close();// Đóng kết nối
        return list;
    }
    public boolean Insert(Message obj) throws SQLException {
        connection = JDBCModel.Connect();
        String sql = "INSERT INTO Messages VALUES(?,?,?,?,?,?,?,?,?)";
        ps = connection.prepareStatement(sql);
        ps.setString(1, obj.getMessage());
        ps.setString(2, obj.getIdNguoiGui());
        ps.setString(3, obj.getTenNguoiGui());
        ps.setString(4, obj.getKieuTinNhan());
        ps.setString(5, obj.getIdNguoiNhan());
        ps.setString(6, obj.getTenNguoiNhan());
        ps.setString(7, obj.getThoiGianGui());
        ps.setString(8, obj.getTrangThai());
        ps.setInt(9, obj.getCountChuaDoc());

        if (ps.executeUpdate() > 0) { // Dùng lệnh executeUpdate cho các lệnh CRUD
            connection.close();
            return true;
        } else {
            connection.close();
            return false;
        }
    }
}
