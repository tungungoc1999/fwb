package com.example.fwbchat.Entity;

public class Group {
    protected String id;
    protected String TenNhom;
    protected String NgayTao;
    protected String Type;
    protected String IdNguoiTao;
    protected String TenNguoiTao;
    protected String Mota;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenNhom() {
        return TenNhom;
    }

    public void setTenNhom(String tenNhom) {
        TenNhom = tenNhom;
    }

    public String getNgayTao() {
        return NgayTao;
    }

    public void setNgayTao(String ngayTao) {
        NgayTao = ngayTao;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getIdNguoiTao() {
        return IdNguoiTao;
    }

    public void setIdNguoiTao(String idNguoiTao) {
        IdNguoiTao = idNguoiTao;
    }

    public String getTenNguoiTao() {
        return TenNguoiTao;
    }

    public void setTenNguoiTao(String tenNguoiTao) {
        TenNguoiTao = tenNguoiTao;
    }

    public String getMota() {
        return Mota;
    }

    public void setMota(String mota) {
        Mota = mota;
    }

    public Group() {
    }

    public Group(String id, String tenNhom) {
        this.id = id;
        TenNhom = tenNhom;
    }

    public Group(String id, String tenNhom, String ngayTao, String type, String idNguoiTao, String tenNguoiTao, String mota) {
        this.id = id;
        TenNhom = tenNhom;
        NgayTao = ngayTao;
        Type = type;
        IdNguoiTao = idNguoiTao;
        TenNguoiTao = tenNguoiTao;
        Mota = mota;
    }

    @Override
    public String toString() {
        return this.TenNhom;
    }
}