package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.UserModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.sql.SQLException;

public class UpdateProfile extends AppCompatActivity {
    private TextInputEditText etHoTen,etQuanHe,etDiaChi,etSDT,etNoiLamViec,etGioiThieu;
    private MaterialButton btnSaveProfile;
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private UserModel userModel = new UserModel();
    private User user = new User();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        context = UpdateProfile.this;
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        editor = sharedPref.edit();
        String uid = sharedPref.getString("uid","");
        try {
            user = userModel.getUserById(uid);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        etHoTen = findViewById(R.id.etHoTen);
        etQuanHe = findViewById(R.id.etQuanHe);
        etDiaChi = findViewById(R.id.etDiaChi);
        etSDT = findViewById(R.id.etSDT);
        etNoiLamViec = findViewById(R.id.etNoiLamViec);
        etGioiThieu = findViewById(R.id.etGioiThieu);
        btnSaveProfile = findViewById(R.id.btnSaveProfile);

        etHoTen.setText(user.getName());
        etQuanHe.setText(user.getRelationship());
        etDiaChi.setText(user.getAddress());
        etSDT.setText(user.getPhoneNumber());
        etNoiLamViec.setText(user.getWorkplace());
        etGioiThieu.setText(user.getIntroduce());


        btnSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String hoten = etHoTen.getText().toString();
                String quanhe = etQuanHe.getText().toString();
                String diachi = etDiaChi.getText().toString();
                String sdt = etSDT.getText().toString();
                String noilamviec = etNoiLamViec.getText().toString();
                String gioithieu = etGioiThieu.getText().toString();
                if(hoten.isEmpty()){
                    Toast.makeText(UpdateProfile.this,"Họ tên không được để trống!" , Toast.LENGTH_LONG).show();
                }else{
                    try {
                        boolean check = userModel.updateProfile(uid,hoten,quanhe,diachi,sdt,noilamviec,gioithieu);
                        if(check == true){
                            Toast.makeText(UpdateProfile.this,"Cập nhật hồ sơ thành công!" , Toast.LENGTH_LONG).show();
                            startActivity(new Intent(UpdateProfile.this,HomeActivity.class));
                        }else {
                            Toast.makeText(UpdateProfile.this,"Cập nhật hồ sơ thất bại!" , Toast.LENGTH_LONG).show();
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }

                }
            }
        });
    }
    public void backHome(View view) {
        startActivity(new Intent(UpdateProfile.this,HomeActivity.class));
    }
}