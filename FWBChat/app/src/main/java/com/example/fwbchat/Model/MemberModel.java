package com.example.fwbchat.Model;

import com.example.fwbchat.DAO.JDBCModel;
import com.example.fwbchat.Entity.Member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MemberModel {
    private JDBCModel jdbcController = new JDBCModel();
    private Connection connection;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public MemberModel() {
        connection = JDBCModel.Connect(); // Tạo kết nối tới database
    }
    public ArrayList<Member> getListMemberGroup(String idN) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<Member> listM = new ArrayList<>();
        String sql = "SELECT * FROM [FWB].[dbo].[MemberGroup] WHERE IdNhom = ?";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        ps.setString(1, idN);
        rs = ps.executeQuery();
        while (rs.next()) {
            listM.add(new Member(rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5)));// Đọc dữ liệu từ ResultSet
        }
        connection.close();// Đóng kết nối
        return listM;
    }
    public void addMemberToGroup(String IdNhom ,String IdThanhVien,String ChucVu ,String NgayTao ) throws SQLException {
        connection = JDBCModel.Connect() ;
        String sql = "INSERT INTO [MemberGroup] Values (?,?,?,?);";

        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        ps.setString(1, IdNhom);
        ps.setString(2, IdThanhVien);
        ps.setString(3, ChucVu);
        ps.setString(4, NgayTao);
        ps.executeUpdate();

    }
    public void deleteMemberToGroup(String IdNhom ,String IdThanhVien) throws SQLException {
        connection = JDBCModel.Connect() ;
        String sql = "DELETE MemberGroup WHERE IdNhom = ? AND IdThanhVien = ?;";

        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        ps.setString(1, IdNhom);
        ps.setString(2, IdThanhVien);
        ps.executeUpdate();

    }
}