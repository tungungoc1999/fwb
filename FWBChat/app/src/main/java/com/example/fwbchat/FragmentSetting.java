package com.example.fwbchat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import com.example.fwbchat.Adapter.MessageAdapter;
import com.example.fwbchat.Adapter.UserAdapter;
import com.example.fwbchat.Entity.Message;
import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.MessageModel;
import com.example.fwbchat.Model.UserModel;

import java.sql.SQLException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentSetting extends Fragment {
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting,container,false);
        TextView tv_name = view.findViewById(R.id.tv_name_user);
        SharedPreferences preferences = this.getActivity().getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        String name = preferences.getString("ten", "");
        tv_name.setText(name);
        return view;
    }
}
