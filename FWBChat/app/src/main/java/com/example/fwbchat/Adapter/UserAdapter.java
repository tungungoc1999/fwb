package com.example.fwbchat.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.fwbchat.Entity.User;
import com.example.fwbchat.R;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<User> {
    private Context context;
    private int resource;
    private ArrayList<User> ar;
    public UserAdapter(Context context, int simple_list_item_1, ArrayList<User> countryList) {
        super(context, 0, countryList);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
//        if (convertView == null) {
//            convertView = LayoutInflater.from(getContext()).inflate(
//                    R.layout.item_user, parent, false
//            );
//        }
        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.item_user, parent, false
        );
        User currentItem = getItem(position);
        if(currentItem !=null){
            TextView txtUserName = convertView.findViewById(R.id.txt_user_name);
            txtUserName.setText(currentItem.getName());
        }
        return convertView;
    }
}
