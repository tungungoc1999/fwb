package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fwbchat.Model.GroupModel;
import com.example.fwbchat.Model.MessageModel;

import java.sql.SQLException;

public class Create_Group extends AppCompatActivity {
    private EditText txtName;
    private Button btnCreate;
    private GroupModel DAO = new GroupModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__group);
        txtName = findViewById(R.id.group_name);
        btnCreate = findViewById(R.id.btn_create_group);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtName.getText().toString().trim();
                if(name.isEmpty()){
                    Toast.makeText(Create_Group.this,"Tên nhóm không được để trống" , Toast.LENGTH_LONG).show();
                }else{
                    SharedPreferences preferences = getSharedPreferences("userLogin", Context.MODE_PRIVATE);
                    String uidUser = preferences.getString("uid","");
                    String nameUser = preferences.getString("ten","");
                    try {
                        boolean check = DAO.addGroup(name,"2021-02-26 13:47:00.000","Private",uidUser,nameUser);
                        if(check == true){
                            Toast.makeText(Create_Group.this,"Tạo nhóm thành công" , Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Create_Group.this,HomeActivity.class));
                        }
                        else{
                            Toast.makeText(Create_Group.this,"Tạo nhóm thất bại" , Toast.LENGTH_LONG).show();
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });
    }
}