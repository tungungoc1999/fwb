package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.fwbchat.DAO.JDBCModel;
import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.UserModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout inputTaiKhoan,inputMatKhau;
    private TextInputEditText uidTaiKhoan,uidMatKhau;
    private MaterialButton loginBtn;
    private ProgressBar progressBar;
    private UserModel DAO = new UserModel();
    private User user = new User();
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        inputTaiKhoan = findViewById(R.id.inputTaiKhoan);
//        inputMatKhau= findViewById(R.id.inputMatKhau);

        uidTaiKhoan = findViewById(R.id.etTaiKhoan);
        uidMatKhau = findViewById(R.id.etMatKhau);
        loginBtn = findViewById(R.id.login);

        progressBar = findViewById(R.id.loginProgress);
        context  = LoginActivity.this;
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        editor = sharedPref.edit();

        //lan dau vao check xem da dang nhap hay chua
        String uid = sharedPref.getString("uid","");
        if(uid != ""){
            Toast.makeText(LoginActivity.this,"Đăng nhập thành công" , Toast.LENGTH_LONG).show();
            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
        }

        //su kien login
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String taikhoan = uidTaiKhoan.getText().toString().trim();
                String matkhau = uidMatKhau.getText().toString().trim();
                if (taikhoan.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Tài khoản không được để trống!", Toast.LENGTH_LONG).show();
                }else if (matkhau.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Mật khẩu không được để trống!", Toast.LENGTH_LONG).show();
                }else{
                    try {
                        user = DAO.checkLogin(taikhoan,matkhau);
                        if(user.getUid() != "" && user.getUid() != null){
                            Toast.makeText(LoginActivity.this,"Đăng nhập thành công" , Toast.LENGTH_LONG).show();
                            editor.putString("uid",user.getUid());
                            editor.putString("ten",user.getName());
                            editor.commit();
                            progressBar.setVisibility(View.VISIBLE);
                            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                        }else{
                            Toast.makeText(LoginActivity.this,"Sai tài khoản hoặc mật khẩu" , Toast.LENGTH_LONG).show();
                        }
                    } catch (SQLException throwables) {
                        Toast.makeText(LoginActivity.this,"Lỗi đăng nhập", Toast.LENGTH_LONG).show();
//                        throwables.printStackTrace();
                    }
                }
            }
        });
    }

    //su kien vao trang dang ky
    public void createUser(View view) {
        startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
    }
}
