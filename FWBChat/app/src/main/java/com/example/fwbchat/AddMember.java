package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.MemberModel;
import com.example.fwbchat.Model.UserModel;

import java.sql.SQLException;
import java.util.ArrayList;

public class AddMember extends AppCompatActivity {
    private UserModel userModel = new UserModel();
    private MemberModel memberModel = new MemberModel();
    private Context context = AddMember.this;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private ArrayList<User> lstUser = new ArrayList<User>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        String idGroup = sharedPref.getString("idGroup","");
        try {
            lstUser = userModel.getUserAddGroup(idGroup);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        ListView myListView = findViewById(R.id.listMemberAdd);
        ArrayAdapter adapter = new ArrayAdapter(AddMember.this, android.R.layout.simple_list_item_1,lstUser);
        myListView.setAdapter(adapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = lstUser.get(position);
                try {
                    memberModel.addMemberToGroup(idGroup,user.getUid(),"User","2021-02-26 13:47:00.000");
                    Toast.makeText(AddMember.this,"Thêm thành viên thành công" , Toast.LENGTH_LONG).show();
                    startActivity(new Intent(AddMember.this,AddMember.class));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }
    public void backToDetail(View view) {
        startActivity(new Intent(AddMember.this,DetailGroup.class));
    }
}