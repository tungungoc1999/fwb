package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.GroupModel;
import com.example.fwbchat.Model.MemberModel;
import com.example.fwbchat.Model.UserModel;

import java.sql.SQLException;
import java.util.ArrayList;

public class DeleteMember extends AppCompatActivity {
    private Context context = DeleteMember.this;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private GroupModel groupModel = new GroupModel();
    private MemberModel memberModel = new MemberModel();
    private UserModel userModel = new UserModel();
    private ArrayList<User> lstUser = new ArrayList<User>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_member);
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        String idGroup = sharedPref.getString("idGroup","");
        String uid = sharedPref.getString("uid","");
        try {
            lstUser = userModel.getUserDeleteGroup(idGroup,uid);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        ListView myListView = findViewById(R.id.listMemberDelete);
        ArrayAdapter adapter = new ArrayAdapter(DeleteMember.this, android.R.layout.simple_list_item_1,lstUser);
        myListView.setAdapter(adapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = lstUser.get(position);
                try {
                    memberModel.deleteMemberToGroup(idGroup,user.getUid());
                    Toast.makeText(DeleteMember.this,"Xóa thành viên thành công" , Toast.LENGTH_LONG).show();
                    startActivity(new Intent(DeleteMember.this,DeleteMember.class));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }
    public void backToDetailH(View view) {
        startActivity(new Intent(DeleteMember.this,DetailGroup.class));
    }
}