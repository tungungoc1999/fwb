package com.example.fwbchat.Model;

import com.example.fwbchat.DAO.JDBCModel;
import com.example.fwbchat.Entity.Group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GroupModel {
    private JDBCModel jdbcController = new JDBCModel();
    private Connection connection;
    PreparedStatement ps = null;
    ResultSet rs = null;
    public GroupModel() {
        connection = JDBCModel.Connect(); // Tạo kết nối tới database
    }
    public ArrayList<Group> getListGroup() throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<Group> list = new ArrayList<>();
        String sql = "SELECT * FROM [Group]";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        rs = ps.executeQuery(sql);
        while (rs.next()) {
            list.add(new Group(rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7)));// Đọc dữ liệu từ ResultSet
        }
        connection.close();// Đóng kết nối
        return list;
    }
    public ArrayList<Group> getListGroup(String uid) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<Group> list = new ArrayList<>();
        String sql = "SELECT * FROM [Group] WHERE Id in  (SELECT MemberGroup.IdNhom FROM MemberGroup WHERE MemberGroup.IdThanhVien = ?)";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        ps.setString(1, uid);
        rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Group(rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7)));// Đọc dữ liệu từ ResultSet
        }
        connection.close();// Đóng kết nối
        return list;
    }
    public ArrayList<Group> findByName(String str) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<Group> listG = new ArrayList<>();
        String sql = "SELECT * FROM [FWB].[dbo].[Group] WHERE HoTen like '%?%';";
        ps = connection.prepareStatement(sql);
        ps.setString(1, str);
        rs = ps.executeQuery();

        while (rs.next()) {
            listG.add(  new Group(  rs.getString(1),
                    rs.getString(2)));
        }
        return listG;
    }
    public Group findById(String str) throws SQLException {
        connection = JDBCModel.Connect();
        Group result = new Group();
        String sql = "SELECT * FROM [FWB].[dbo].[Group] WHERE Id = ?;";
        ps = connection.prepareStatement(sql);
        ps.setString(1, str);
        rs = ps.executeQuery();

        while (rs.next()) {
            result = new Group(rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7));
        }
        return result;
    }
    public boolean addGroup(String TenNhom ,String NgayTao,String Type ,String IdNguoiTao ,String TenNguoiTao ) throws SQLException {
        connection = JDBCModel.Connect() ;
        String sql = "INSERT INTO [Group] VALUES (?,?,?,?,?,?);";

        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        ps.setString(1, TenNhom);
        ps.setString(2, NgayTao);
        ps.setString(3, Type);
        ps.setString(4, IdNguoiTao);
        ps.setString(5, TenNguoiTao);
        ps.setString(6, "");
        int i = ps.executeUpdate();
        if(i > 0){
            String queryLast = "SELECT * FROM [Group] WHERE ID = (SELECT MAX(ID) FROM [Group])";
            Group result = null;
            ps = connection.prepareStatement(queryLast);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = new Group(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
            }
            MemberModel memberModel = new MemberModel();
            memberModel.addMemberToGroup(result.getId(),result.getIdNguoiTao(),"Admin",result.getNgayTao());
            return true;
        }
        else return  false;
    }
}
