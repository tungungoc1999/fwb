package com.example.fwbchat.Model;

import com.example.fwbchat.DAO.JDBCModel;
import com.example.fwbchat.Entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserModel {

    private JDBCModel jdbcController = new JDBCModel();
    private Connection connection;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    public UserModel() {
        connection = JDBCModel.Connect(); // Tạo kết nối tới database
    }

    public ArrayList<User> getuserlist() throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM UserInfo";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        ps = connection.prepareStatement(sql);
        rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new User(rs.getString(1), rs.getString(4)));// Đọc dữ liệu từ ResultSet
        }
        connection.close();// Đóng kết nối
        return list;
    }
    public ArrayList<User> getuserlist(String uid) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM UserInfo WHERE Id != ?";
        ps = connection.prepareStatement(sql);
        ps.setString(1, uid);
        rs = ps.executeQuery(); // Thực thi câu lệnh SQL trả về đối tượng ResultSet. // Mọi kết quả trả về sẽ được lưu trong ResultSet
        while (rs.next()) {
            list.add(new User(rs.getString(1), rs.getString(4)));// Đọc dữ liệu từ ResultSet
        }
        connection.close();// Đóng kết nối
        return list;
    }

    public ArrayList<User> findByName(String str) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<User> listU = new ArrayList<>();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM [FWB].[dbo].[UserInfo] WHERE HoTen like '%?%';";
        ps = connection.prepareStatement(sql);
        ps.setString(1, str);
        rs = ps.executeQuery();

        while (rs.next()) {
            listU.add(new User(rs.getString(1),
                    rs.getString(4)));
        }
        return listU;
    }

    public boolean Insert(User objUser) throws SQLException {
        connection = JDBCModel.Connect();

        String sql = "INSERT INTO [UserInfo]([TaiKhoan],[MatKhau],[HoTen]) VALUES (?,?,?);";
        ps = connection.prepareStatement(sql);
        ps.setString(1, objUser.getUsername());
        ps.setString(2, objUser.getPassword());
        ps.setString(3, objUser.getName());
        if (ps.executeUpdate() > 0) { // Dùng lệnh executeUpdate cho các lệnh CRUD
            connection.close();
            return true;
        } else {
            connection.close();
            return false;
        }
    }

    public boolean Update(User objUser) throws SQLException {
        connection = JDBCModel.Connect();

        String sql = "UPDATE UserInfo set HoTen = ? where ID = ?";
        ps = connection.prepareStatement(sql);
        ps.setString(1, objUser.getName());
        ps.setString(2, objUser.getUid());
        if (ps.executeUpdate(sql) > 0) {
            connection.close();
            return true;
        } else
            connection.close();
        return false;
    }

    public boolean Delete(User objUser) throws SQLException {
        connection = JDBCModel.Connect();
        String sql = "DELETE FROM USER WHERE Id = ?";
        ps = connection.prepareStatement(sql);
        ps.setString(1, objUser.getUid());
        if (ps.executeUpdate(sql) > 0){
            connection.close();
            return true;
        }

        else
            connection.close();
        return false;
    }
    public User findById(String uid) throws SQLException {
        connection = JDBCModel.Connect();
        User result = new User();
        String sql = "SELECT * FROM UserInfo WHERE Id= ?";
        ps = connection.prepareStatement(sql);
        ps.setString(1,uid);
        rs = ps.executeQuery();
        while (rs.next()) {
            result = new User(rs.getString(1), rs.getString(4));// Đọc dữ liệu từ ResultSet
        }
        connection.close();
        return result;
    }
    public boolean checkRegister(String name) throws SQLException {
        connection = JDBCModel.Connect();
        User result = new User();
        String sql = "SELECT * FROM UserInfo WHERE TaiKhoan= ?";
        ps = connection.prepareStatement(sql);
        ps.setString(1,name);
        rs = ps.executeQuery();
        while (rs.next()) {
            result = new User(rs.getString(1), rs.getString(4));// Đọc dữ liệu từ ResultSet
        }
        connection.close();
        if(result.getUid() != "" && result.getUid() != null){
            return false;
        }else{
            return true;
        }
    }
    public User checkLogin(String taikhoan,String matkhau) throws SQLException {
        connection = JDBCModel.Connect();
        User result = new User();
        String sql = "SELECT * FROM UserInfo WHERE [TaiKhoan] = ? AND [MatKhau] = ? ;";
        ps = connection.prepareStatement(sql);
        ps.setString(1,taikhoan);
        ps.setString(2,matkhau);
        rs = ps.executeQuery();
        while (rs.next()) {
            result = new User(rs.getString(1), rs.getString(4));// Đọc dữ liệu từ ResultSet
        }
        connection.close();
        return result;
    }
    public User getUserById(String uid) throws SQLException {
        connection = JDBCModel.Connect();
        User result = new User();
        String sql = "SELECT * FROM UserInfo WHERE Id= ?";
        ps = connection.prepareStatement(sql);
        ps.setString(1,uid);
        rs = ps.executeQuery();
        while (rs.next()) {
            result = new User(rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getString(9),
                    rs.getString(10),
                    rs.getString(11));// Đọc dữ liệu từ ResultSet
        }
        connection.close();
        return result;
    }
    public ArrayList<User> getUserAddGroup(String str) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<User> listU = new ArrayList<>();
        Statement statement = connection.createStatement();
        String sql = " SELECT * FROM UserInfo WHERE Id NOT IN (SELECT DISTINCT IdThanhVien FROM MemberGroup WHERE IdNhom = ?);";
        ps = connection.prepareStatement(sql);
        ps.setString(1, str);
        rs = ps.executeQuery();

        while (rs.next()) {
            listU.add(new User(rs.getString(1),
                    rs.getString(4)));
        }
        return listU;
    }
    public ArrayList<User> getUserDeleteGroup(String idGroup,String idUserLogin) throws SQLException {
        connection = JDBCModel.Connect();
        ArrayList<User> listU = new ArrayList<>();
        Statement statement = connection.createStatement();
        String sql = " SELECT * FROM UserInfo WHERE Id IN (SELECT DISTINCT IdThanhVien FROM MemberGroup WHERE IdNhom = ?) AND Id != ?;";
        ps = connection.prepareStatement(sql);
        ps.setString(1, idGroup);
        ps.setString(2, idUserLogin);
        rs = ps.executeQuery();

        while (rs.next()) {
            listU.add(new User(rs.getString(1),
                    rs.getString(4)));
        }
        return listU;
    }
    public boolean updateProfile(String id , String hoten , String moiquanhe,String diachi , String sodienthoai , String noilamviec , String gioithieu)throws SQLException {
        connection = JDBCModel.Connect();
        String sql = "UPDATE UserInfo set HoTen = ? , MoiQuanHe = ? , NoiSong = ? , SoDT = ? , NoiLamViec = ? , GioiThieu = ? WHERE Id = ?";
        ps = connection.prepareStatement(sql);
        ps.setString(1, hoten);
        ps.setString(2, moiquanhe);
        ps.setString(3, diachi);
        ps.setString(4, sodienthoai);
        ps.setString(5, noilamviec);
        ps.setString(6, gioithieu);
        ps.setString(7, id);
        int check = ps.executeUpdate();
        if(check > 0){
            return  true;
        }
        else {
            return false;
        }
    }
}