package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fwbchat.Adapter.MessageAdapter;
import com.example.fwbchat.Entity.Message;
import com.example.fwbchat.Model.MessageModel;

import net.sourceforge.jtds.jdbc.DateTime;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ChatWithConversation extends AppCompatActivity {
    private ArrayList<Message> mlist;
    private MessageAdapter mAdapter;
    private MessageModel DAO = new MessageModel();
    private ImageView btnSend;
    private TextView textMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_with_conversation);
        TextView tv_name = findViewById(R.id.tv_name);
        ListView lv1= findViewById(R.id.list_mess);
        btnSend = findViewById(R.id.ivSend);
        textMessage = findViewById(R.id.etComposeBox);
        mlist = new ArrayList<Message>();
        lv1.setDivider(null);
        lv1.setDividerHeight(0);
        SharedPreferences preferences = getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        String uid = preferences.getString("uid","");
        String name = preferences.getString("ten","");
        String idConversation = preferences.getString("idConversation","");
        String nameConversation = preferences.getString("nameConversation","");

        tv_name.setText(nameConversation);
        try {
            if(uid !="") {
                mlist = DAO.getListMessage(uid,idConversation,"User");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        mAdapter = new MessageAdapter(ChatWithConversation.this, mlist);
        lv1.setAdapter(mAdapter);
        lv1.setSelection(mAdapter.getCount()-1);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mess = textMessage.getText().toString();
                if(!mess.isEmpty()){
                    Message obj = new Message();
                    obj.setIdNguoiGui(uid);
                    obj.setTenNguoiGui(name);
                    obj.setIdNguoiNhan(idConversation);
                    obj.setTenNguoiNhan(nameConversation);
                    obj.setMessage(mess);
                    obj.setKieuTinNhan("User");
                    obj.setThoiGianGui("2021-02-26 13:47:00.000");
                    obj.setTrangThai("Đã xem");
                    obj.setCountChuaDoc(1);
                    try {
                        if(DAO.Insert(obj)){
                            mlist = DAO.getListMessage(uid,idConversation,"User");
                            mAdapter = new MessageAdapter(ChatWithConversation.this, mlist);
                            lv1.setAdapter(mAdapter);
                            lv1.setSelection(mAdapter.getCount()-1);
                            textMessage.setText("");
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });
    }
    public void backToListConversation(View view) {
        startActivity(new Intent(ChatWithConversation.this,HomeActivity.class));
    }
}