package com.example.fwbchat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;

import com.example.fwbchat.Adapter.UserAdapter;
import com.example.fwbchat.Entity.Group;
import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.GroupModel;
import com.example.fwbchat.Model.UserModel;

import java.sql.SQLException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentGroup extends ListFragment {
    private ArrayList<Group> mlist;
    private UserAdapter mAdapter;
    private View view;
    private GroupModel DAO = new GroupModel();
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mlist = new ArrayList<Group>();
        SharedPreferences preferences = this.getActivity().getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        String uid = preferences.getString("uid", "");
        try {
            if (uid != "") {
                mlist = DAO.getListGroup(uid);
            } else {
                mlist = DAO.getListGroup();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, mlist);
        setListAdapter(adapter);
        return inflater.inflate(R.layout.list_item_group, container, false);
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = getActivity().getSharedPreferences("userLogin", MODE_PRIVATE);
        editor = sharedPref.edit();
        Group user = mlist.get(position);
        editor.putString("idGroup", user.getId());
        editor.putString("nameGroup", user.getTenNhom());
        editor.commit();
        Intent intent = new Intent(getActivity(), ChatWithGroup.class);
        startActivity(intent);
        super.onListItemClick(l, v, position, id);
    }
}
