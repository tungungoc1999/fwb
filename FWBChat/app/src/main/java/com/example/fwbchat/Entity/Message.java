package com.example.fwbchat.Entity;

import org.json.JSONObject;

import java.util.List;

public class Message {
    protected String id;
    protected String message;
    protected String IdNguoiGui;
    protected String TenNguoiGui;
    protected String KieuTinNhan;
    protected String IdNguoiNhan;
    protected String TenNguoiNhan;
    protected String ThoiGianGui;
    protected String TrangThai;
    protected int CountChuaDoc;

    public Message() {
    }

    public Message(String id, String message, String idNguoiGui, String tenNguoiGui, String kieuTinNhan, String idNguoiNhan, String tenNguoiNhan, String thoiGianGui, String trangThai, int countChuaDoc) {
        this.id = id;
        this.message = message;
        IdNguoiGui = idNguoiGui;
        TenNguoiGui = tenNguoiGui;
        KieuTinNhan = kieuTinNhan;
        IdNguoiNhan = idNguoiNhan;
        TenNguoiNhan = tenNguoiNhan;
        ThoiGianGui = thoiGianGui;
        TrangThai = trangThai;
        CountChuaDoc = countChuaDoc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdNguoiGui() {
        return IdNguoiGui;
    }

    public void setIdNguoiGui(String idNguoiGui) {
        IdNguoiGui = idNguoiGui;
    }

    public String getTenNguoiGui() {
        return TenNguoiGui;
    }

    public void setTenNguoiGui(String tenNguoiGui) {
        TenNguoiGui = tenNguoiGui;
    }

    public String getKieuTinNhan() {
        return KieuTinNhan;
    }

    public void setKieuTinNhan(String kieuTinNhan) {
        KieuTinNhan = kieuTinNhan;
    }

    public String getIdNguoiNhan() {
        return IdNguoiNhan;
    }

    public void setIdNguoiNhan(String idNguoiNhan) {
        IdNguoiNhan = idNguoiNhan;
    }

    public String getTenNguoiNhan() {
        return TenNguoiNhan;
    }

    public void setTenNguoiNhan(String tenNguoiNhan) {
        TenNguoiNhan = tenNguoiNhan;
    }

    public String getThoiGianGui() {
        return ThoiGianGui;
    }

    public void setThoiGianGui(String thoiGianGui) {
        ThoiGianGui = thoiGianGui;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String trangThai) {
        TrangThai = trangThai;
    }

    public int getCountChuaDoc() {
        return CountChuaDoc;
    }

    public void setCountChuaDoc(int countChuaDoc) {
        CountChuaDoc = countChuaDoc;
    }
}
