package com.example.fwbchat.Entity;

import org.json.JSONObject;

import java.util.List;

public class User {
    protected String uid;
    protected String username;
    protected String password;
    protected String name;
    protected String status;
    protected String lastActiveAt;
    protected String relationship;
    protected String phoneNumber;
    protected String address ;
    protected String workplace ;
    protected String introduce  ;


    public User() {
    }

    public User(String uid, String name) {
        this.uid = uid;
        this.name = name;
    }

    public User(String uid, String username, String password, String name, String status, String lastActiveAt, String relationship, String phoneNumber, String address, String workplace, String introduce) {
        this.uid = uid;
        this.username = username;
        this.password = password;
        this.name = name;
        this.status = status;
        this.lastActiveAt = lastActiveAt;
        this.relationship = relationship;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.workplace = workplace;
        this.introduce = introduce;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastActiveAt() {
        return lastActiveAt;
    }

    public void setLastActiveAt(String lastActiveAt) {
        this.lastActiveAt = lastActiveAt;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setHabitat(String habitat) {
        this.address = habitat;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
