package com.example.fwbchat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.UserModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.sql.SQLException;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout inputTaiKhoan,inputMatKhau,inputHoTen;
    private TextInputEditText uidTaiKhoan,uidMatKhau,uidHoTen;
    private MaterialButton registerBtn;
    private UserModel DAO = new UserModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        inputTaiKhoan = findViewById(R.id.inputTaiKhoan_r);
        inputMatKhau= findViewById(R.id.inputMatKhau_r);
        inputHoTen= findViewById(R.id.inputHoten_r);
        uidTaiKhoan = findViewById(R.id.etTaiKhoan_r);
        uidMatKhau = findViewById(R.id.etMatKhau_r);
        uidHoTen = findViewById(R.id.etHoTen_r);
        registerBtn = findViewById(R.id.register);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String taikhoan = uidTaiKhoan.getText().toString();
                String matkhau = uidMatKhau.getText().toString();
                String hoten = uidHoTen.getText().toString();
                try {
                    if(DAO.checkRegister(taikhoan) == true){
                        User user = new User();
                        user.setUsername(taikhoan);
                        user.setPassword(matkhau);
                        user.setName(hoten);
                        if(DAO.Insert(user)){
                            Toast.makeText(RegisterActivity.this, "Đăng ký thành công!", Toast.LENGTH_LONG).show();
                            loginUser(view);
                        }else{
                            Toast.makeText(RegisterActivity.this, "Đăng ký thất bại!", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(RegisterActivity.this, "Tài khoản đã tồn tại! Vui lòng nhập lại!", Toast.LENGTH_LONG).show();
                    }
                } catch (SQLException throwables) {
                    Toast.makeText(RegisterActivity.this, "Lỗi đăng ký!", Toast.LENGTH_LONG).show();
                    throwables.printStackTrace();
                }
            }
        });
    }
    public void loginUser(View view) {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
    }
}
