package com.example.fwbchat.Entity;

public class Member {
    protected String Id;
    protected String IdNhom;
    protected String IdThanhVien;
    protected String ChucVu;
    protected String NgayTao;

    public Member() {
    }

    public Member(String id, String idNhom, String idThanhVien, String chucVu, String ngayTao) {
        Id = id;
        IdNhom = idNhom;
        IdThanhVien = idThanhVien;
        ChucVu = chucVu;
        NgayTao = ngayTao;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getIdNhom() {
        return IdNhom;
    }

    public void setIdNhom(String idNhom) {
        IdNhom = idNhom;
    }

    public String getIdThanhVien() {
        return IdThanhVien;
    }

    public void setIdThanhVien(String idThanhVien) {
        IdThanhVien = idThanhVien;
    }

    public String getChucVu() {
        return ChucVu;
    }

    public void setChucVu(String chucVu) {
        ChucVu = chucVu;
    }

    public String getNgayTao() {
        return NgayTao;
    }

    public void setNgayTao(String ngayTao) {
        NgayTao = ngayTao;
    }

}
