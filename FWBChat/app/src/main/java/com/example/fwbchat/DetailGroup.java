package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fwbchat.Entity.Group;
import com.example.fwbchat.Entity.Member;
import com.example.fwbchat.Entity.User;
import com.example.fwbchat.Model.GroupModel;
import com.example.fwbchat.Model.MemberModel;
import com.example.fwbchat.Model.UserModel;
import com.google.android.material.appbar.MaterialToolbar;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailGroup extends AppCompatActivity {
    private TextView tv_group_name,group_description,tv_delete,tv_exit;
    private Context context = DetailGroup.this;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private GroupModel groupModel = new GroupModel();
    private MemberModel memberModel = new MemberModel();
    private UserModel userModel = new UserModel();
    private RelativeLayout rl_add_member,rl_delete_member;
    private MaterialToolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_group);
        tv_group_name = findViewById(R.id.tv_group_name);
        group_description = findViewById(R.id.group_description);
        rl_add_member = findViewById(R.id.rl_add_member);
        rl_delete_member = findViewById(R.id.rl_delete_member);
        tv_delete = findViewById(R.id.tv_delete);
        tv_exit = findViewById(R.id.tv_exit);
        toolbar = findViewById(R.id.groupDetailToolbar);

        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        String idGroup = sharedPref.getString("idGroup","");
        String uid = sharedPref.getString("uid","");
//        String nameGroup = sharedPref.getString("nameGroup","");
        Group group = new Group();
        ArrayList<Member> lstMem = new ArrayList<Member>();
        ArrayList<User> lstUser = new ArrayList<User>();
        try {
             group = groupModel.findById(idGroup);
            lstMem = memberModel.getListMemberGroup(group.getId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        for(Member item : lstMem){
            try {
                lstUser.add(userModel.getUserById(item.getIdThanhVien()));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if(group != null){
            tv_group_name.setText(group.getTenNhom());
            group_description.setText(group.getMota());
            if(!group.getIdNguoiTao().equals(uid)){
                rl_add_member.setVisibility(View.GONE);
                tv_delete.setVisibility(View.GONE);
                rl_delete_member.setVisibility(View.GONE);
            }
        }
//        ListView myListView = findViewById(R.id.member_list);
//        ArrayAdapter adapter = new ArrayAdapter(DetailGroup.this, android.R.layout.simple_list_item_1,lstUser);
//        myListView.setAdapter(adapter);

        rl_add_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailGroup.this,AddMember.class));
            }
        });
        rl_delete_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailGroup.this,DeleteMember.class));
            }
        });
        tv_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    memberModel.deleteMemberToGroup(idGroup,uid);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                Toast.makeText(DetailGroup.this,"Rời nhóm thành công" , Toast.LENGTH_LONG).show();
                startActivity(new Intent(DetailGroup.this,HomeActivity.class));
            }
        });
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailGroup.this,ChatWithGroup.class));
            }
        });
    }
}