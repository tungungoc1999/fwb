USE [FWB]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 06/06/2021 23:41:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenNhom] [nvarchar](50) NULL,
	[NgayTao] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[IdNguoiTao] [int] NULL,
	[TenNguoiTao] [nvarchar](50) NULL,
	[MoTa] [nvarchar](100) NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemberGroup]    Script Date: 06/06/2021 23:41:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdNhom] [int] NULL,
	[IdThanhVien] [int] NULL,
	[ChucVu] [nvarchar](50) NULL,
	[NgayVao] [nvarchar](50) NULL,
 CONSTRAINT [PK_MemberGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Messages]    Script Date: 06/06/2021 23:41:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Messages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[IdNguoiGui] [nvarchar](50) NULL,
	[TenNguoiGui] [nvarchar](50) NULL,
	[KieuTinNhan] [nvarchar](50) NULL,
	[IdNguoiNhan] [nvarchar](50) NULL,
	[TenNguoiNhan] [nvarchar](50) NULL,
	[ThoiGianGui] [datetime] NULL,
	[TrangThai] [nvarchar](50) NULL,
	[CountChuaDoc] [nvarchar](50) NULL,
 CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserInfo]    Script Date: 06/06/2021 23:41:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaiKhoan] [varchar](50) NULL,
	[MatKhau] [varchar](50) NULL,
	[HoTen] [nvarchar](50) NULL,
	[TrangThai] [nvarchar](50) NULL,
	[lastActive] [datetime] NULL,
	[MoiQuanHe] [nvarchar](50) NULL,
	[SoDT] [nvarchar](11) NULL,
	[NoiSong] [nvarchar](50) NULL,
	[NoiLamViec] [nvarchar](50) NULL,
	[GioiThieu] [nvarchar](100) NULL,
 CONSTRAINT [PK_UserInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Group] ON 

INSERT [dbo].[Group] ([Id], [TenNhom], [NgayTao], [Type], [IdNguoiTao], [TenNguoiTao], [MoTa]) VALUES (1, N'NhomBTL', N'2021-02-26 13:47:00.000', N'Private', 4, N'Nvt_1', N'')
SET IDENTITY_INSERT [dbo].[Group] OFF
GO
SET IDENTITY_INSERT [dbo].[MemberGroup] ON 

INSERT [dbo].[MemberGroup] ([Id], [IdNhom], [IdThanhVien], [ChucVu], [NgayVao]) VALUES (1, 1, 4, N'Admin', N'2021-02-26 13:47:00.000')
INSERT [dbo].[MemberGroup] ([Id], [IdNhom], [IdThanhVien], [ChucVu], [NgayVao]) VALUES (3, 1, 2, N'User', N'2021-02-26 13:47:00.000')
INSERT [dbo].[MemberGroup] ([Id], [IdNhom], [IdThanhVien], [ChucVu], [NgayVao]) VALUES (4, 1, 3, N'User', N'2021-02-26 13:47:00.000')
SET IDENTITY_INSERT [dbo].[MemberGroup] OFF
GO
SET IDENTITY_INSERT [dbo].[Messages] ON 

INSERT [dbo].[Messages] ([Id], [Message], [IdNguoiGui], [TenNguoiGui], [KieuTinNhan], [IdNguoiNhan], [TenNguoiNhan], [ThoiGianGui], [TrangThai], [CountChuaDoc]) VALUES (1, N'Hello bạn', N'4', N'Nvt_1', N'User', N'5', N'Nvt2', CAST(N'2021-02-26T13:47:00.000' AS DateTime), N'Đã xem', N'1')
INSERT [dbo].[Messages] ([Id], [Message], [IdNguoiGui], [TenNguoiGui], [KieuTinNhan], [IdNguoiNhan], [TenNguoiNhan], [ThoiGianGui], [TrangThai], [CountChuaDoc]) VALUES (2, N'Cho mình làm quen', N'4', N'Nvt_1', N'User', N'5', N'Nvt2', CAST(N'2021-02-26T13:47:00.000' AS DateTime), N'Đã xem', N'1')
INSERT [dbo].[Messages] ([Id], [Message], [IdNguoiGui], [TenNguoiGui], [KieuTinNhan], [IdNguoiNhan], [TenNguoiNhan], [ThoiGianGui], [TrangThai], [CountChuaDoc]) VALUES (3, N'Hello mn', N'4', N'Nvt_1', N'Group', N'1', N'NhomBTL', CAST(N'2021-02-26T13:47:00.000' AS DateTime), N'Đã xem', N'1')
INSERT [dbo].[Messages] ([Id], [Message], [IdNguoiGui], [TenNguoiGui], [KieuTinNhan], [IdNguoiNhan], [TenNguoiNhan], [ThoiGianGui], [TrangThai], [CountChuaDoc]) VALUES (4, N'Demo BTL nào ', N'4', N'Nvt_1', N'Group', N'1', N'NhomBTL', CAST(N'2021-02-26T13:47:00.000' AS DateTime), N'Đã xem', N'1')
INSERT [dbo].[Messages] ([Id], [Message], [IdNguoiGui], [TenNguoiGui], [KieuTinNhan], [IdNguoiNhan], [TenNguoiNhan], [ThoiGianGui], [TrangThai], [CountChuaDoc]) VALUES (5, N'Ok bạn ơi', N'1', N'Đặng Trần Tú', N'Group', N'1', N'NhomBTL', CAST(N'2021-02-26T13:47:00.000' AS DateTime), N'Đã xem', N'1')
SET IDENTITY_INSERT [dbo].[Messages] OFF
GO
SET IDENTITY_INSERT [dbo].[UserInfo] ON 

INSERT [dbo].[UserInfo] ([Id], [TaiKhoan], [MatKhau], [HoTen], [TrangThai], [lastActive], [MoiQuanHe], [SoDT], [NoiSong], [NoiLamViec], [GioiThieu]) VALUES (1, N'dangtrantu', N'123456', N'Đặng Trần Tú', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserInfo] ([Id], [TaiKhoan], [MatKhau], [HoTen], [TrangThai], [lastActive], [MoiQuanHe], [SoDT], [NoiSong], [NoiLamViec], [GioiThieu]) VALUES (2, N'nguyenquyminh', N'123456', N'Nguyễn Quý Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserInfo] ([Id], [TaiKhoan], [MatKhau], [HoTen], [TrangThai], [lastActive], [MoiQuanHe], [SoDT], [NoiSong], [NoiLamViec], [GioiThieu]) VALUES (3, N'luongvanthanh', N'123456', N'Lương Văn Thanh', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserInfo] ([Id], [TaiKhoan], [MatKhau], [HoTen], [TrangThai], [lastActive], [MoiQuanHe], [SoDT], [NoiSong], [NoiLamViec], [GioiThieu]) VALUES (4, N'nguyenvanthanh_test1', N'123456', N'Nvt_1', NULL, NULL, N'Nguoi co tinh yeu', N'0123456789', N'Dan Phuong', N'Ha Noi', N'Dep trai cao to 1m75')
INSERT [dbo].[UserInfo] ([Id], [TaiKhoan], [MatKhau], [HoTen], [TrangThai], [lastActive], [MoiQuanHe], [SoDT], [NoiSong], [NoiLamViec], [GioiThieu]) VALUES (5, N'nguyenvanthanh_test2', N'123456', N'Nvt2', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserInfo] OFF
GO
